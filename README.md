# Le chiffrement de César

Défi réalisé dans le cadre de la formation en Dart proposé par Purple Giraffe.

Le projet à été réalisé en Dart et est uniquement utilisable en console

* Le programme choisi la clé de cryptage, un chiffre entre 1 et 5. Cette clé est utilisée pour paramétrer le décalage
* Les caractères sont transformé en valeur ASCII, le décalage est reporté sur la table ASCII
* Les caractères non-imprimables de la table ASCII sont exclus.
* Les premiers caractères de la table ASCII, en fonction de la clé de cryptage, sont reportés sur les derniers caractères imprimables de la table ASCII.