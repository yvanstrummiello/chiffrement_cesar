import 'dart:math';

import 'NE_PAS_TOUCHER/user_input.dart';

void main() {
  print("Chiffrement de Ceasar");
  var random = Random();
  final cleChoisie = random.nextInt(5) + 1;
  final messageACoder = saisirMessageAChiffrer();
  String messageCode = encryptageCesar(messageACoder, cleChoisie);

  print("Voici votre message codé :");
  print(messageCode);
  print("Votre clé de chiffrement est : $cleChoisie");
}

//fonction de saisie du message à encoder
String saisirMessageAChiffrer() {
  String message = readText("Saississez le message à encoder :");
  return message;
}

//fonction encryptage
String encryptageCesar(String messageUtilisateur, int cleEncodage) {
  String messageCode = "";
  int caractereMinTableASCII = 32;
  int sautCaractereASCIIinferieur = 95;

  List<int> messageConvertiASCII = [];
  List<int> messageEncodeASCII = [];

  // récupération des caractères et encodage en ascii
  for (int lettre = 0; lettre < messageUtilisateur.length; lettre++) {
    messageConvertiASCII.add(messageUtilisateur.codeUnitAt(lettre));
  }

  // application du décalage dans le tableau converti
  for (var codeASCII in messageConvertiASCII) {
    if (codeASCII < caractereMinTableASCII + cleEncodage) {
      messageEncodeASCII
          .add(codeASCII - cleEncodage + sautCaractereASCIIinferieur);
    } else {
      messageEncodeASCII.add(codeASCII - cleEncodage);
    }
  }

  // affichage du message
  for (var codeASCIIEncode in messageEncodeASCII) {
    messageCode += String.fromCharCode(codeASCIIEncode);
  }

  return messageCode;
}
